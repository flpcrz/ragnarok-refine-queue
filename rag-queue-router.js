import express, { Router, Request } from 'express';
import admin from 'firebase-admin';
const PubSub = require(`@google-cloud/pubsub`);

admin.initializeApp({
    credential: admin.credential.applicationDefault()
});

const pubsub = new PubSub();
const router = Router()

const subscriptionName = 'projects/ragnarok-db/subscriptions/last-refines-count';
const timeout = 60;

const subscription = pubsub.subscription(subscriptionName);
const decoder = new StringDecoder('utf8');

router.get("/", async (req, res, next) => {
    try {
        let messageCount = 0;
        let lastMessages = [];

        const messageHandler = message => {
            console.log(`Mensagens recebida: ${message.id}:`);
            console.log(`\tDados: ${message.data}`);
            lastMessages.push(message.data.toString('utf8'));
            messageCount += 1;

            message.ack();
        };

        subscription.on(`message`, messageHandler);
        setTimeout(function () {
            subscription.removeListener("message", messageHandler);
            console.log(`${messageCount} message(s) received.`);
            res.json({
                totalMessages: messageCount,
                messages: lastMessages
            });
        }, timeout * 200);
    } catch (e) {
        next(e);
    }
});

export default router;