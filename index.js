import app from "./rag-queue-api";

function App(req, res) {
    if (!req.url) {
        req.url = '/';
        req.path = '/';
    }
    return app(req, res);
}

const lastWeaponsRefine = App;

export { lastWeaponsRefine }